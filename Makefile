serve:
	git submodule init 
	git submodule update
	hugo serve

resources:
	cd terraform
	terraform init
	terraform apply -var-file=prod.tfvars
	cd -
