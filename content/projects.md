---
title: "PROJECTS"
date: 2019-10-18T09:37:50+02:00
---

# PROJECTS @ HOME


## The Time I Accidentally Built My Own "Cloud" Platform

During the pandemic I leveraged the fact that I'm having deployed hardware on three different, geo-distributed *regions* so I semi-consciounsly built my own highly-available, on-premise "Cloud" platform.

I used existing software components I already have knowledge in and additionally wrote some software myself. Basic building blocks, such as blob storage, databases, service discovery and Kubernetes nodes are highly-available through (almost) immutable VMs based on a terraformed libvirt API.

[![imagen](/img/soeren-platform.drawio.png)](/img/soeren-platform.drawio.png)

### SELF-WRITTEN SOFTWARE
- [Manage SSH (host) certificates using Hashicorp Vault](https://github.com/soerenschneider/ssh-key-signer)
- [Manage client certificates using Hashicorp Vault](https://github.com/soerenschneider/vault-pki-cli)
- [Automatically manage Vault AppRoles](https://github.com/soerenschneider/scripts/blob/main/vault/vault_approle_cli.py)
- [Automatically unlock stuff using Vault](https://github.com/soerenschneider/occult)
- [Request x509 certs via LetsEncrypt and distribute via Vault to private hosts](https://github.com/soerenschneider/acmevault)
- [Detect IP changes and update predefined DNS records through signed messages via MQTT](https://github.com/soerenschneider/dyndns)

### APPS LAYER
- Highly available monitoring running Prometheus stack
- Federated Prometheus cluster on top of Cortex
- Logging based on Loki

### PLATFORM LAYER
- Running a Hashicorp Vault cluster configured using [terraform](https://github.com/soerenschneider/tf-vault). Used for 
  - [machine identities](https://www.vaultproject.io/docs/secrets/identity), the foundation of every authentication and authorization
  - internal PKIs, using x509 for client and host certificates
  - SSH certificates, all SSH authentication is based on certificates instead of authorized keys
  - static secrets, such as API tokens
  - AWS secrets engine, dynamically create short-lived access tokens
  - ... 
- Highly available Minio Cluster running in distributed mode, installed using Ansible, configured via Terraform
- Running highly available MariaDB Galera cluster, configured using Ansible
- Single Sign On provided by Keycloak using OIDC, configured by terraform. Not highly available, yet
- Running highly available Kubernetes cluster installed using Kubespray, configured using GitOps
- Service discovery based on Consul, experiments ongoing in regards of Ansible dynamic inventory

### INFRASTRUCTURE LAYER

#### OpenBSD Router
- All routers are built on top of [APU2](https://www.pcengines.ch/apu2.htm) boards, running [resflash](https://gitlab.com/bconway/resflash) to provide (almost) immutable infrastructure
- Routers configured using Ansible

#### Linux Virtual Server Hosts

- The host systems expose a minimal attack surface and only run libvirtd and a sshd server
- Automatic patch management in place
- Virtual machines are solely configured using [terraform](https://github.com/soerenschneider/tf-libvirt)
- Each machine has an associated [identity](https://www.vaultproject.io/docs/secrets/identity) enabling zero trust networking


## Notification on Late Trams

While I was living in Cologne I realized the local public transportation company enjoyed being.. on the spontaneous side of life, so I created an overengineered solution to notify me on late trams. I ended up with 4 microservices that communicate via loosely coupled streams based on redis.

- [scraping tram data](https://gitlab.com/soerenschneider/kfailb-scraper)
- [parsing scraped data](https://gitlab.com/soerenschneider/kfailb-parser)
- [deduplication and notifying on late tram via telegram bot](https://gitlab.com/soerenschneider/kfailb-bot)
- [storing data long term](https://gitlab.com/soerenschneider/k-fail-banal)

## Home Automation

Instead of relying on a commercial vendor, I spent some time on my DIY home automation based on Home Assistant and my fleet of Raspberry Zeros, Arduinos & ESP2866s. I've re-written tools based on the excellent gobot framework to scrape and distribute all the sensor data:

- [reading motion data](https://github.com/soerenschneider/gobot-pir)
- [reading temperature data](https://github.com/soerenschneider/gobot-bme280/)
- [reading brightness data](https://github.com/soerenschneider/gobot-lux)
- [(deprecated) reading motion sensor data](https://gitlab.com/soerenschneider/pir-motion-raspberry)
- [(deprecated) reading temperature sensor data](https://gitlab.com/soerenschneider/mqtt-tempsensor)
- [(deprecated) reading arbitrary analog sensor data over serial line](https://gitlab.com/soerenschneider/serial-sensor-reader).


# PROJECTS @ WORK

### Chaos Engineering
Co-founder of the Chaos Engineering guild that serves as force for the adoption of Chaos Engineering principles at our employer, Rewe Digital. We evaluate tools in the context of Chaos Engineering, provide introduction lessons & workshops, do consultation work and moderate our teams' Gamedays. After hosting several Gamedays for other teams, we also started offering Firedrills, based on common scenarios across our tech-stack (how much is your backup scenario worth if you don't know if it works?). Further, we started finding the biggest risks of our own components by performing SFMEAs and plan to offer SFMEA consulting for other teams as well.

### GitHub Enterpise Migration
We worked on the conception & realization of a modern, scalable, self-hosted GitHub Actions Runner infrastructure. Building and operating a service that regularly rotates individual runner tokens for all business units. Providing customized tooling (Terraform & POSIX) to support our engineers to migrate more than 3500 repositories from Bitbucket to GitHub with minimal friction.

### Google IAP Kubernetes Controller
To allow an uncomplicated exposal of Kubernetes services via a secured Google Identity Aware Proxy, we wrote a Kubernetes Controller that solely works on appropriate labels on the K8s service resources. To be 100% sure that the service is not being exposed publicly (in case Google IAP has trouble), an [envoy proxy](https://www.envoyproxy.io/) is injected as a sidecar that performs mandatory JWT verification.

### Synchronizing secrets from Hashicorp Vault
We wrote a critical component that reads secrets from Hashicorp Vault and synchronizes them unidirectionally to interchangeable backend. It is possible to consume (re-read) a certain secret on each interval or reading the secret once and extending its lease variably before its expiry. The provided backends currently support writing the secret as a Kubernetes secret and writing as a file on Cloud Storage bucket.

### Graceful Pod Killer
> There are 2 hard problems in computer science: cache invalidation, naming things, and off-by-1 errors

I wrote a tool that is able to safely restart targeted Deployments. Targets can be auto-discovered by (multiple) labels, a certain secret they have mounted and/or their start date. After killing a pod, it waits until the replica is healthy again to continue with the next pod in line. This tool comes in handy for controlled chaos engineering operations or when you need to restart pods to force the consumption of an updated secret. 

### Automatically verifying Cloud SQL backups 
On more than one occasion we were facing incidents with Cloud SQL instances that got into a bad state and whose last *n* out of *m* backups were unusable. We decided to build a product using multiple Cloud Functions that automatically restores new Cloud SQL backups, verifies them, cleans up everything and notifies the owning team in case of problems.

### Get notified on outdated Docker images in Kubernetes
During several 'Learning Friday' sessions I wrote a POC that checks whether running pods use outdated docker images. To inspect a cluster, an agent is deployed that gathers the needed information via internal Kubernetes API calls and reports back via Pub Sub to the master. In the master, the docker image string is disassembled. After that, a call against the appropriate image registry's API is performed, checking out the available versions and writes the information to a SQL database. Versions were compared and on a version skew, teams were notified via prometheus.

### Cloud Functions metrics exporter
When using Cloud Functions we wanted to be able to use fine-grained monitoring for our functions. Due to the nature of Cloud Functions still being early beta, it wasn't possible to peer them to our GKE network. Therefore we weren't able to use Prometheus Pushgateway (as it is exposed only inside the clusters) or scrape them like regular targets (due to their low runtime, networking, ...).

As the functions were all triggered using Pub Sub, we decided to write a Prometheus exporter that receives metrics in raw Prometheus text format dispatched via Pub Sub once the function finished.

### AWS Cloud Architecture & Migration
The most challenging task undoubtedly was planning and migrating to the cloud architecture at AWS. Due to multiple constellations, the end date of the migration was already known before real planning started. After _a lot_ of small, nightly iterations the transition was done, but not the work. The initial move involved almost only manual work, after the move to AWS I started to replicate the environment using IAC.

With an ever increasing size of both team members and services, conflicts of the monolithic architecture became apparent. The first container orchestration used, was ECS. Due to its nature, being as dull as solid, it is the ideal solution for understaffed departments. Later on, there was the big shift towards a kops-created Kubernetes, unfortunately operated only by me.

### Business Partner Data Curation

Concept and realization of an easily configurable, high-performance concurrent service that improves business partner data. The data was read from a queue, ran through several *curation elements* and was written to a new MongoDB collection. The list of elements and their order was dymanic and interchangable. A single data element could take various paths down the curator based on its attributes, e.g. a certain country, a certain "legal form". A curation element could alter (e.g. transliteration) and also flag (e.g. "legal form is invalid for given country") data. Several permutations of configuration could be performed, to find the best settings for input data.

### Providing scalable, enterprise-ready Semantic MediaWiki stacks

Using both CloudFormation and Ansible I was able to roll-out a scalable [Semantic Media Wiki](https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki) providing Single Sign On capabilities with few effort. The resulting VMs were part of Auto Scaling Groups and were backed by Docker images. SPARQL queries were answered by an Apache Fuseki.
