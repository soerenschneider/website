---
title: "ABOUT ME"
date: 2018-03-28T19:58:44+02:00
---

Hi! After living in Köln for almost ten years, I recently moved further up the Rhine to Düsseldorf. I'm currently working as a Senior Cloud Engineer, but I'm kind of a *jack of all trades*, having worked in several similar positions and generally being interested in various technical topics.

I also like to invest a good portion of my spare time on technology: coding, reading technical books, running (semi-)professional home labs, trying to keep up with new technologies or paradigms – or short: practicing continuous improvement. Also, I'm a Linux and open source advocate. After figuring out I've been using Linux [for two decades](https://en.opensuse.org/Archive:SuSE_Linux_7.0), I realized I'm kind of old.

Topics unrelated to tech that I care about:
- [Music](https://www.last.fm/user/bob_terwilliger)
- [Cooking](https://yummy.soerenschneider.com)
- Taking photos
- Literature
- Hiking, running, playing football and recently yoga as well
- Tea! I accumulated quite a collection of Chinese, Japanese and Taiwanese teas
