---
title: "CV"
date: 2018-11-01T14:44:35+02:00
categories: ["professional"]
---

# ABSTRACT

- **10+ years** of working experience in the industry
- Started out as **Software-Engineer** and later **Software-Architect**, working more closely to **Cloud Infrastructures** for the last 5+ years
- Generally interested in Software Resilience, Distributed Systems and IT Security
- Knowledge (different levels) in AWS, Azure & Google Cloud Platform
- Writing software preferably in Golang and Python
- Currently getting deeper into Azure and trying to tackle the steep learning curve of Rust's lifetimes

<br/>
<br/>

# [SENIOR CLOUD ENGINEER @ SAP FIONEER (Remote)](https://www.sapfioneer.com/), 12/2021 -- NOW

### CORE OBJECTIVES
- Help shaping and implementing the MVP of SAP Fioneer's *Commercial Lending* product

### TASKS
- Involved in building the company's upcoming Azure architecture
- Realized secrets management based on Azure Key Vault, GitHub Actions and Terraform

### TECHNOLOGIES

* Golang, Python
* Azure
  * Kubernetes w. Istio / AKS
  * Azure SQL (Postgres)
  * AAD
* GraphQL, REST, OData
* Terraform
* GitHub Actions
  * Snyk Container
  * Codacy
* SAP BTP
* Kotlin w. Spring Boot


# [CLOUD ENGINEER @ RD (Cologne / Hybrid)](https://www.rewe-digital.com/), 11/18

After 4 years, I moved on to work as a Cloud Engineer at Rewe Digital, helping the company digitize the retail food sector in Germany. I was eager to both expand my Cloud expertise on the Google Cloud Platform and also grow with a dedicated expert team of Cloud Engineers.

### CORE OBJECTIVES
1. Building, continously improving and maintaining the company's large fulfillment platform
1. Enabling and offering consultation to feature teams to efficiently run their payloads on our platform

### TASKS
- Establishing Chaos Engineering and SRE-methologies within the company
  - Performing Chaos Engineering gamedays, both internally and with fellow teams
  - Risk management using SFMEA
- Providing building blocks that solve common platform problems
  - Both dogfooding and relying on existing, battle-proven open source software
- Offering trainings for feature teams
  - Google Cloud Platform 101
  - Kubernetes 101
  - Chaos Engineering
- Performed smoothless migration & integration from Bitbucket to Github Enterprise Integration
  - Providing a modern & scalable locally hosted GitHub Actions Runners setup

### TECHNOLOGIES

* Golang, Python, Shell scripts
* Google Cloud
  * Kubernetes (GKE)
  * GCE
  * IAP
  * Cloud SQL
  * Cloud Functions
  * Datastore
  * Pub Sub
* Vault
* Terraform, Packer, Ansible
* Prometheus-stack, Grafana
* GitHub Enterprise, GitHub Actions, Bitbucket, Jenkins
* Litmus Chaos, ChaosMesh, Chaos Monkey for Spring Boot

----

# [CLOUD OPERATIONS MANAGER @ CDQ (Remote)](https://www.cdq.ch/data-sharing/data-sharing-community), 01/17--11/18

When Startups don't offer monetary compensation, they'll compensate with funny titles on business cards and a steep learning curve. From January 2017 on, I was promoted internally to be the sole responsible person for migrating our cloud presence to AWS and establishing a professional, modern cloud architecture built on AWS.

### CORE OBJECTIVES
1. Professionalizing the company's cloud infrastructure
1. Supporting and thus facilitating the company's rapid growth by providing highly scalable infrastructure

### RESPONSIBILITIES / TASKS

* Sole responsible person for providing and operating a scalable, robust platform
    * Planned and realized AWS cloud architecture
    * Performed migration from Swisscom Enterprise Cloud Solution to AWS
    * Providing dynamic testing environments using Terraform
    * Analyzing performance-, security- and network-related issues
    * Monitoring infrastructure- and service health
* Containerized all workloads
* Driving cloud contextual topics at the Architecture Board
    * Getting rid of state
    * Dealing with network failures
    * Self-healing software
* Training for team members
    * Kubernetes basics
    * Building Docker images correctly
    * How to define Prometheus metrics & alerts
* Infrastructure documentation and compliance
* Realization, verification and instrumentation of backups
* Building and providing developer selfservices
* Cost management

### TECHNOLOGIES

* Python
* AWS
  - Lambda
  - RDS
  - EC2
  - ECS
  - S3
  - (EKS)
* Kubernetes (Kops), Kong API Gateway, Ansible
* Graylog, Prometheus, Alertmanager, Grafana
* Jenkins, Sonarqube, Nexus

----

# [SOFTWARE ARCHITECT @ CDQ (Remote)](https://www.cdq.ch/data-sharing/data-sharing-community) 01/15--01/17

In January 2015 I started working for the Switzerland-based startup whose vision is to improve master data quality related to business partners. As I was the first technical employee, I translated and realized all initial business requirements as a working software solution that proved itself over the years. The resulting SaaS platform allowed corporations to share and thus improve their data. Later, more and more related services, such as bank fraud detection and -prevention, were added to the product. 

### CORE OBJECTIVES
1. Delivering a MVP of the data sharing SaaS product
1. Establishing professional software development structures
1. Defininition and implementation of the architecture
1. Gather and translate business requirements into technical solutions

### RESPONSIBILITIES / TASKS

* Creating a MVP of the data sharing SaaS product
  * Defining SaaS components
  * Providing Swisscom Enterprise Cloud Infrastructure
  * API Design
  * Domain model design
  * Database design
* Translating business requirements into technical solutions
  * Writing epics, user stories and decision records
  * Defining architecture and appropriate documentation
* Defining software development structures
  * Introduced Kanban, alter Scrum
  * SCM branching processes and release management processes
* Building CI Pipelines
  * Providing software testing stack via Docker compose
  * SonarQube ownership
* Documentation of software architecture and architectural choices
* Realiziation of smaller side-projects

### TECHNOLOGIES

* Java, Spring (-boot, -data, -security, -cloud, -batch), Hibernate, RabbitMQ
* Swisscom Enterprise Cloud, Apache Tomcat
* Jenkins, Bamboo, SonarKube, Docker
* MySQL, MediaWiki, MongoDB, Redis, ElasticSearch, OpenLDAP, simpleSAML, OpenVPN

----

# [SOFTWARE DEVELOPER @ OpenCMS (Cologne)](http://www.opencms.org/en/), 06/13--12/14

Being driven by the idealistic motive of getting paid working on Open Source software, I got my first full-time job working as a software developer on OpenCMS. My main tasks included

* Working on OpenCMS
* Various works around the Solr-powered full text search module
* Creation of a full text search web framework using handlebars, GWT and SOLR
* Realization of smaller projects
* Initiated first internal tests with Docker

#### TECHNOLOGIES

* Java, Maven, JavaScript, Handlebars, Google Web Toolkit
* Apache Solr
* Docker

----

# [RESEARCH ASSISTANT @ Fraunhofer IESE (Kaiserslautern)](https://www.iese.fraunhofer.de/), 08--13

* Gained first experiences working in a professional environment
* Worked in the context of the European research project [AdiWa](http://publica.fraunhofer.de/documents/N-175840.html) on a Java-based backend, using REST, Hibernate and MQTT.
* Worked with [model based code generation](https://www.eclipse.org/modeling/emf/) on [internal architecture tools](https://www.iese.fraunhofer.de/en/competencies/architecture/architecture-tools.html)
* Implementation and research of small prototypes
* Data analysis

----

# EDUCATION

* M.Sc, INFORMATION SYSTEMS, 2013, [Live-visualization of source code artifacts for a software engineering-demonstrator
](http://publica.fraunhofer.de/documents/N-330459.html)

* B.Sc, COMPUTER SCIENCE, 2009, [Design and automation of a release engineering process
](http://publica.fraunhofer.de/documents/N-192404.html)
